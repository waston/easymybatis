/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.oschina.durcframework.easymybatis.query;

public class QueryConfig {
	
	/**
	 * 每页记录条数
	 */
	private static int DEFAULT_PAGE_SIZE = 10;
	
	public static int getDefaultPageSize() {
		return DEFAULT_PAGE_SIZE;
	}
	
	public static void setDefaultPageSize(int defaultPageSize) {
		if(defaultPageSize < 1) {
			throw new IllegalArgumentException("setDefaultPageSize(int defaultPageSize)参数不能小于1");
		}
		DEFAULT_PAGE_SIZE = defaultPageSize;
	}
}
