/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.oschina.durcframework.easymybatis.ext.code.util;

import java.util.HashMap;
import java.util.Map;

/**
 * 字段类型转换
 */
public class SqlTypeUtil {
	private static Map<String, String> javaBoxTypeMap = new HashMap<String, String>();
	private static Map<String, String> mybatisTypeMap = new HashMap<String, String>();

	static {
		
		
		// "int","double","long","short","byte","boolean","char","float"
		javaBoxTypeMap.put("int", "Integer");
		javaBoxTypeMap.put("double", "Double");
		javaBoxTypeMap.put("long", "Long");
		javaBoxTypeMap.put("short", "Short");
		javaBoxTypeMap.put("byte", "Byte");
		javaBoxTypeMap.put("boolean", "Boolean");
		javaBoxTypeMap.put("char", "Character");
		javaBoxTypeMap.put("float", "Float");
  
/*
Java Type			JDBC Type     
---------			--------------
String				VARCHAR       
java.math.BigDecimal		DECIMAL       
boolean				BIT           
byte				TINYINT       
short				SMALLINT      
int				INTEGER       
long				BIGINT        
float				REAL          
double				DOUBLE        
byte[]				BINARY        
java.sql.Date			DATE          
java.sql.Time			TIME          
java.sql.Timestamp		TIMESTAMP     
Clob				CLOB          
Blob				BLOB          
Array				ARRAY         
mapping of underlying type	DISTINCT        
Struct				STRUCT        
*/		
		mybatisTypeMap.put("Integer", "INTEGER");
		mybatisTypeMap.put("Double", "DOUBLE");
		mybatisTypeMap.put("Float", "FLOAT");
		mybatisTypeMap.put("Long", "BIGINT");
		mybatisTypeMap.put("Short", "SMALLINT");
		mybatisTypeMap.put("Byte", "TINYINT");
		mybatisTypeMap.put("Character", "VARCHAR");
		mybatisTypeMap.put("BigDecimal", "DECIMAL");
		mybatisTypeMap.put("byte[]", "BINARY");
		mybatisTypeMap.put("Date", "TIMESTAMP");
		mybatisTypeMap.put("Time", "TIME");
		mybatisTypeMap.put("Timestamp", "TIMESTAMP");
		mybatisTypeMap.put("String", "VARCHAR");
        
	}
	
	
	
	
	/**
	 * 将Java字段类型转换为java装箱字段类型
	 * @param javaType
	 * @return 找不到类型默认返回String
	 */
	public static String convertToJavaBoxType(String javaType){
		String javaBoxType = javaBoxTypeMap.get(javaType);
		return javaBoxType == null ? javaType : javaBoxType;
	}
	
	/**
	 * 将Java字段类型转换为mybatis的jdbcType
	 * @param javaType
	 * @return 找不到类型默认返回VARCHAR
	 */
	public static String convertToMyBatisJdbcType(String javaType){
		javaType = convertToJavaBoxType(javaType);
		String mybatisJdbcType = mybatisTypeMap.get(javaType);
		return mybatisJdbcType == null ? "VARCHAR" : mybatisJdbcType;
	}
	
}
